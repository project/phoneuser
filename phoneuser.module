<?php

/**
 * @file
 *
 * This module allows people to have basic user accounts with a phone number and without an e-mail.
 *
 */

/**
 * Implements hook_field_extra_fields()
 */
function phoneuser_field_extra_fields() {
  $extra['node']['event'] = array(
    'display' => array(
      'phoneuser_subscribe_form_display' => array(
        'label' => t('Get updates'),
        'description' => t('Enter your cell phone number'),
        'weight' => 0,
      ),
    ),
  );
  return $extra;
}


/**
 * Implements hook_node_view().
 *
 * Needed for phoneuser_field_extra_fields to have any effect.
 */
function phoneuser_node_view($node, $view_mode, $langcode) {
  // If this is not a session node type, bail immediately.
  if ($node->type != 'event') {
    return;
  }

  $node->content['phoneuser_subscribe_form_display'] = phoneuser_subscribe_form_display($node);

}


/**
 * Display the cell phone number subscribe/signup form.
 */
function phoneuser_subscribe_form_display($node) {
  $build = array();
  $form = drupal_get_form('phoneuser_subscribe_form', $node);
  $build['form'] = $form;
  $build['extra'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('Your cell phone number will not be shown to anyone. You can unsubscribe at any time.') . '</p>',
  );
  return $build;
}


/**
 * A form for providing a cell phone number.
 */
function phoneuser_subscribe_form($form, &$form_state, $node) {
  $form['#attributes'] = array(
    'class' => array(
      'form-inline'
    )
  );
  $form['cell_number'] = array(
    '#type' => 'textfield',
    '#placeholder' => t('Enter your mobile number'),
    '#size' => 25,
    '#maxlength' => 18,
  );
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Get updates by text message'),
    '#attributes' => array(
      'class' => array(
        'btn',
        'btn-primary',
      ),
    ),
  );
  return $form;
}


/**
 * Validate that the provided phone number is conceivably functional.
 */
function phoneuser_subscribe_form_validate($form, &$form_state) {
  $cell_number = $form_state['values']['cell_number'];
  $cell_number = preg_replace("/[^0-9,.]/", "", $cell_number);
  $len = strlen($cell_number);
  // We are not using phone.module's valid_phone_number() function because it
  // requires the country code
  if ($len < 10 || $len > 15) {
    form_set_error('cell_number', t('A valid phone number must be at least ten digits and not more than fifteen.  Please use numbers and not letters.'));
  }
  else {
    // Assign cleaned-up phone number back to its form value for use in submit.
    $form_state['values']['cell_number'] = $cell_number;
  }
}


/**
 * Submit the phone number, creating a user if necessary.
 */
function phoneuser_subscribe_form_submit($form, &$form_state) {
  $cell_number = $form_state['values']['cell_number'];
  $nid = $form_state['values']['nid'];
  // If the number length is exactly 10, add a 1 for entitled North Americans.
  if (strlen($cell_number) === 10) {
    $cell_number = '1' . $cell_number;
  }
  $account = new StdClass();
  $account->is_new = TRUE;
  $account->status = TRUE;
  // Create a random username for the participant.
  $account->name = 'p' . $nid . date('ymdHis') . rand(0, 99);
  $account->pass = '';
  $account->mail = '';
  $account->init = '';
  $account->roles = array(DRUPAL_AUTHENTICATED_RID => 'authenticated user');
  $account->field_voipnumber[LANGUAGE_NONE][0] = array(
    'vnid' => $cell_number,
    'advanced' => array(
      'type' => VOIPNUMBER_MOBILE,
      'country' => 'United States',
      'default' => TRUE,
    ),
  );
//  $account->field_first_name[LANGUAGE_NONE][0]['value'] = 'first name';
//  $account->field_last_name[LANGUAGE_NONE][0]['value'] = 'last name';
  $new_user = user_save($account);
  $node = node_load($nid);
  phoneuser_register_user_for_event($new_user, $node);
  phoneuser_confirm_signup($node, $new_user);

}


/**
 * Create a registration association of a user with an event (aka session).
 */
function phoneuser_register_user_for_event($account, $entity) {
  // Hardcode event entity type to node for now.  Dear Drupal, an entity's type
  // should always be easily discoverable from the entity object.
  $entity_type = 'node';
  list($entity_id) = entity_extract_ids($entity_type, $entity);
  if (registration_status($entity_type, $entity_id)) {
    $registration_type = registration_get_entity_registration_type($entity_type, $entity);
    $registration = entity_get_controller('registration')->create(array(
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
      'type' => $registration_type,
    ));

  }
  else {
    drupal_set_message(t('Sorry, registrations are no longer available for %name',
      array('%name' => entity_label($entity_type, $entity))), 'error');
    return;
  }

  $wrapper = entity_metadata_wrapper('registration', $registration);
  // $user = $wrapper->user->value();
  $host = $wrapper->entity->value();
  $state = $wrapper->state->value();

  $settings = registration_entity_settings($registration->entity_type, $registration->entity_id);
  $capacity = $settings['capacity'];
  $remaining = $capacity - registration_event_count($registration->entity_type, $registration->entity_id);

  // Limit is only used if you want to let someone reserve extra spaces.  We aren't doing that right now.
  // $limit = isset($settings['settings']['maximum_spaces']) ? $settings['settings']['maximum_spaces'] : 1;
  $count = 1;
  $has_room = registration_has_room($registration->entity_type, $registration->entity_id, $count, $registration->registration_id);
  if (!$has_room) {
    form_set_error('', t('Sorry, unable to register for %label due to: insufficient spaces remaining.',
      array('%label' => $label)));
  }
  // debug("this is the best $host->type");

  // Check if the current user is already registered.
  // registration_is_registered($registration, NULL, $user->uid)

  // Save the registration.
  $registration->user_uid = $account->uid;
  $registration->state = 'complete';  // @TODO create on install, make a constant.

  // Save the registration and redirect.
  if (registration_save($registration)) {
    $reg_config = registration_entity_settings($registration->entity_type, $registration->entity_id);
    if (!empty($reg_config['settings']['confirmation']) && strlen($reg_config['settings']['confirmation'])) {
      drupal_set_message(t($reg_config['settings']['confirmation']));
    }
  }
  else {
    drupal_set_message(t('There was a problem submitting your registration.'), 'error');
  }
}


/**
 * Send a text message to the user letting them know they have signed up.
 *
 * @param object $node  The session node.
 * @param object $account  A user object.
 */
function phoneuser_confirm_signup($node, $account) {
  $message = t('Thank you for subscribing to updates for @title. To unsubscribe text reply "u @nid"', array('@title' => $node->title, '@nid' => $node->nid));
  $blast_id = $node->nid;
  $options = array(
    'tags' => array('subscribe'),
  );
  // The "fully-loaded $user object" user_save() is supposed to return isn't.
  $subscriber = user_load($account->uid);
  $real_vnid = $subscriber->field_voipnumber[LANGUAGE_NONE][0]['real_vnid'];
  $voipnumber = VoipNumber::load($real_vnid);
  // foreach($phone_numbers as $voipnumber) {
  voipnumber_blast_add_text($blast_id, $voipnumber, $message, $options);
  // }
}


/**
 * Implements hook_form_FORMID_alter().
 *
 * Hide option for BoF organizers to disable registration and hide the disabled
 * extension field.
 */
function phoneuser_form_event_node_form_alter(&$form, &$form_state) {
  $form['field_register']['#access'] = FALSE;
  $form['field_session_extension']['#access'] = FALSE;
}


/**
 * Show link to create alert only to session node authors (and superadmins).
 */
function phoneuser_node_view_alter(&$build) {
  $node = $build['#node'];
  if ($node->type != 'event' || $node->status != NODE_PUBLISHED) {
    return;
  }
  $link = l(t('Send an announcement'), 'node/add/announcement',
    array('query' => array('field_event' => $node->nid)));
  $build['announcement_link'] = array(
    '#markup' => $link,
    '#weight' => -100,
    '#access' => phoneuser_send_blast_access($node),
  );
}


/**
 * Return true if user has permission to send blast (author of session node).
 *
 * Superadmins also allowed.
 */
function phoneuser_send_blast_access($node) {
  return node_access('update', $node, $GLOBALS['user']);
}


/**
 * Implement hook_node_submit to send text messages on publish of announcement.
 */
function phoneuser_node_submit($node, $form, &$form_state) {
  // If it's not an announcement node being published, bail immediately.
  if ($node->type != 'announcement' || $node->status != NODE_PUBLISHED) {
    return;
  }
  if (isset($node->original) && $node->original->status != NODE_NOT_PUBLISHED) {
    return;
  }
  $event = node_load($node->field_event[LANGUAGE_NONE][0]);
  $conditions = array(
    'entity_type' => 'node',
    'entity_id' => $event->nid,
  );
  $registrations = registration_load_multiple(array(), $conditions);
  $subscribers = phoneuser_load_registration_users($registrations);
  $text = $node->field_text[LANGUAGE_NONE][0];
  phoneuser_send_announcement_texts($text, $subscribers);
}


/**
 * Load registered users from registration object.
 */
function phoneuser_load_registration_users($registrations) {
  $user_ids = array();
  foreach ($registrations as $registration) {
    $user_ids[] = $registration->user_id;
  }
  return user_load_multiple($user_ids);
}


/**
 * Given text and array of subscribing users, send text blast.
 */
function phoneuser_send_announcement_texts($text, $subscribers) {
  foreach ($subscribers as $subscriber) {
    $real_vnid = $subscriber->field_voipnumber[LANGUAGE_NONE][0]['real_vnid'];
    $voipnumber = VoipNumber::load($real_vnid);
    voipnumber_blast_add_text($blast_id, $voipnumber, $message, $options);
  }
}
