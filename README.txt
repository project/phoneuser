This module allows users to be registered with a phone number rather than an
e-mail.

It is intended that this user be very limited in permissions until they add an
e-mail address.

This module is sponsored by Agaric and meant to play well with VoIP Drupal as
part of the PowerToConnect service and distribution.  For more information, see:

* http://agaric.com
* http://voipdrupal.org
* http://powertoconnect.com

1. INSTALLATION

1.1 Enable the module at /admin/build/modules
1.2 Go to /admin/user/settings, activate and configure the feature

2. OPTIONAL, BUT RECOMMENDED MODULES

We recommend the No Request New Password module be used. It disables
the "request new password" link in the user login block and page.
Download it at: http://drupal.org/project/noreqnewpass

3. TODO LIST

3.1 Implement a way to separate users who have e-mail from the ones who
don’t.

4. ACKNOWLEDGMENT

Thanks Leo Burd and Michele Metts for the inspiration.
